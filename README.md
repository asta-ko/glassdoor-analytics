Glassdoor scraping & data analysis 
======================

Scrapes glassdoor and gets jobs data.
Target job title and country are customizable.
Contains sample csv datasets for data engineer job openings.

```shellsession
docker-compose up
```

